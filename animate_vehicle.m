% animation function to be called after simulation
% draws vehicle and simulation parameters based on histories of simulation parameters
% inputs:
%   T           - simulation time vector
%   X_hist      - vehicle state history
%   Y_hist      - vehicle output history
%   X_c_hist    - control state history
%   Y_c_hist    - control output history
%
%   T_c         - control loop time vector
%   dist_hist   - disturbance history
%   control_params_hist - control params history    
%   control_input_hist - control input history
%
%   vehicle     - struct of vehicle parameters


function [plots] = animate_vehicle(fig_anim,T,X_hist,Y_hist,X_c_hist,Y_c_hist, T_c,dist_hist,control_params_hist,control_input_hist, vehicle)

    % exageration factors,f_ex, for steer angle and slip
    % higher factors make smaller values more obvious
    % x_plot = tanh(f*x*(1/x_max))*x_max
    steer_angle_exag = 30;
    slip_exag = 15;
    % unpack state
    wfl = X_hist(1,:); wfr = X_hist(2,:); wrl = X_hist(3,:); wrr = X_hist(4,:);
    Vx = X_hist(5,:); Vy = X_hist(6,:); omegaz = X_hist(7,:);
    Xt = X_hist(8,:); Yt = X_hist(9,:); Psit = X_hist(10,:);
    Td = X_hist(11:14,:);
    Tb = X_hist(15:18,:);

    % unpack vehicle
    L = vehicle.L;
    B = vehicle.B; Rw = vehicle.Rw; L1 = vehicle.L1; L2 = vehicle.L2;
    m = vehicle.m; Iz = vehicle.Iz; Iw = vehicle.Iw; 
    mu_slip_curve = vehicle.mu_slip_curve;

    % control_input
    deltaf = control_input_hist.deltaf;
    deltar = control_input_hist.deltar;
    Td_des = control_input_hist.Td;
    Tb_des = control_input_hist.Tb;
    abs_flag = control_input_hist.abs_flag;

    %% will become loop
    % fig_anim = figure();
    axis equal;
    hold on;

    %% coordinates and constant values
    %% car
    car_B = [ [0,0] ; [L1,B/2] ; [L1,-B/2] ; [-L2, B/2] ; [-L2, -B/2] ]';
    wheel_offset = .5; % distance from wheel center to car outline

    %% wheels
    % wheelcenters
    wheelcenter_B = car_B(:,2:end) + [ -wheel_offset/2 .* sign(car_B(1,2:end)); wheel_offset.*sign(car_B(2,2:end))];

    %% drivetrain
    % motor center is slightly in front of cg, axle centers are between wheel centers
    drivetrain_B = [ car_B(:,1)+[.4;0] [wheelcenter_B(1,1);0] [wheelcenter_B(1,3);0] ];

    %% slip vis
    % max height of triangle
    slip_vis_max = (L - Rw - wheel_offset)/2 -.1;
    % calculate current slip triangle
    slip_vis_width = B/10;
    % slip triangle coordinates - sfl1,sfl2,sfl3,sfr1,sfr2,sfr3,...
    x1 = [0;+slip_vis_width/2];
    x2 = [0;-slip_vis_width/2];
    % determine wheel widths from maximum cornering stiffness
    front_wheel_width = 10;
    % determine wheel colors from braking/driving torques
    % braking is red, driving is green
    brake_color = [237,43,0]/255; % color at max brake
    drive_color = [84,240,12]/255; % color at max drive
    % use desired/driven to values because of peaks caused by slip starts and stops
    Tb_max = max(Tb_des,[],2) + 5;
    Td_max = max(Td_des,[],2) + 5;

    [~,Cf_max,Cr_max] = mu_slip_curve([0]);

    % calculate magnification factor for simulation
    N = (numel(T)-1)/numel(T_c);
    % controller index
    i_c = 0;
    % speed up animation by skipping states
    speed = 2;
    idx = 1;
    for(i = 1:numel(T));
        if(mod(i-1,N)==0 && i ~=numel(T))
            i_c = i_c + 1;
        end
        if(mod(i,speed)~=1)
        else
            if(mod(T(i),.5)==0) % update title every half second
                fig_anim.Children.Title.String = ['Time: ' sprintf('%0.1f',T(i)) ' sec ' ' V: ' sprintf('%0.2f',sqrt(Vx(i)^2 + Vy(i)^2)) ' m/s'];
            end

            % draw a car easy right
            % coordinates of car in body coordinates
            % car_B is matrix of points:
            %   cg,flcorner,frcorner,rlcorner,rrcorner,
            % wheel_B is matrix of points: 
            %   flwheelcenter,frwheelcenter,rrwheelcenter,rlwheelcenter, ...
            %       flwheelfront,frwheelfront,...... flwheelback,frwheelback,...
            % drivetrain_B is matrix of points comprising drivetrain drawing:
            %   motorcenter,axlefcenter,axlercenter

            %% slip visualization
            slip = Y_hist(1:4,i);
            % see other part of slip triangle for details of x3
            slip_vis_height = slip_vis_max * abs(slip);
            % exagerate slip height
            slip_vis_height = slip_vis_max*tanh(slip_exag*slip_vis_height/slip_vis_max);
            x3 = [-slip_vis_height';zeros(1,4)];
            slip_vis_B = zeros(2,12);
            for(w=1:4)
                % direction of slip
                s_dir = sign(slip(w));
                if(s_dir==0);s_dir=1;end
                i_s = w*3-2;
                % coordinates of slip triangles
                slip_vis_B(:,i_s:i_s+1) = [x1 x2] + wheelcenter_B(:,w) -s_dir*[Rw+.05;0];
                slip_vis_B(:,i_s+2) = x3(:,w) + wheelcenter_B(:,w) -s_dir*[Rw+.05;0] +s_dir*x3(:,w);
            end

            %% wheel - calculate wheel position
            steer_angles = [deltaf(i_c),deltaf(i_c),deltar(i_c),deltar(i_c)];
            % apply exageration to steer angles
            steer_angles = pi/2*tanh(steer_angle_exag*steer_angles/(pi/2));
            wheel_tf = Rw * [cos(steer_angles);sin(steer_angles)]; 
            wheelfront_B = wheelcenter_B + wheel_tf;
            wheelback_B  = wheelcenter_B - wheel_tf;
            wheel_B = [wheelcenter_B wheelfront_B wheelback_B];


            %% perform transformation from B to I
            % rotation tf
            R_BI = [cos(Psit(i)),sin(Psit(i));-sin(Psit(i)),cos(Psit(i))];
            % translation tf, position of B from I expr in I
            T_BI = [Xt(i);Yt(i)];
            % transform car from B to I
            car_I = R_BI' * car_B + T_BI;
            % transform wheels from B to I
            wheel_I = R_BI' * wheel_B + T_BI;
            % drivetrain
            drivetrain_I = R_BI' * drivetrain_B + T_BI;
            % slip vis
            slip_vis_I = R_BI' * slip_vis_B + T_BI;

            %% additional values for plotting
            % package car points for plotting
            car_plot_pts = [car_I(:,2:3) flip(car_I(:,4:5),2) car_I(:,2)];
            % choose wheel colors
            wheel_color = (brake_color.*Tb(:,i)./Tb_max) + (drive_color.*Td(:,i)./Td_max);
            % color limited to between 0 and 1, in case wheel is being driven and braked at same time
            wheel_color = min(wheel_color,ones(4,1));
            % determine wheel widths by current cornering coefficient
            [~,Cf,Cr] = mu_slip_curve(slip);
            % cornering coefficients of each wheel
            CC = [Cf(1:2);Cr(3:4)];
            CC_max = [Cf_max;Cf_max;Cr_max;Cr_max];
            % define width, ranging from 2 to 10
            wheel_width = 8*(CC./CC_max) + 2;

            % drivetrain coloring
            % want same green color to mean driving torque is applied to particular wheel
            dt_color = min(drive_color.*Td_des(:,i_c)./Td_max,ones(4,1));

            % slip coloring
            % red is abs on, blue is abs off
            abs_color = brake_color; abs_off_color = [90,202,219]/255;
            slip_color = [abs_color-abs_off_color].*abs_flag(:,i_c) + abs_off_color;

            %% plots:
            %   car
            %   cg(1) (circle)
            %   cg(2) (cross)
            %   wheel(1) (fl)
            %   wheel(2) (fr)
            %   wheel(3) (rl)
            %   wheel(4) (rr)
            %   drivetrain
            %   slip_vis
            %   dist_forces
            % draw car
            if(~exist('plot_car','var')) % create plot of car, assuming all are created at the same time

                % plot car outline
                plot_car = plot(car_plot_pts(1,:),car_plot_pts(2,:),'DisplayName','car','LineWidth',3,'Color',[.8 .2 .3]);

                % plot cg location
                plot_cg(1) = scatter(car_I(1,1),car_I(2,1),400*(L/diff(xlim)),[.6 .2 .3],'o','DisplayName','cg','LineWidth',2);
                plot_cg(2) = scatter(car_I(1,1),car_I(2,1),400*(L/diff(xlim)),[.6 .2 .3],'+','HandleVisibility','off','LineWidth',2);

                for(w = 1:4)
                    % plot each wheel
                    plot_wheel(w) = plot(wheel_I(1,w+4:4:end),wheel_I(2,w+4:4:end),'DisplayName','wheel','LineWidth',wheel_width(w),'Color',wheel_color(w,:));

                    % plot slip triangle
                    i_s = w*3-2;
                    slip_triangle = [slip_vis_I(:,i_s:i_s+2) slip_vis_I(:,i_s)];
                    plot_slip(w) = plot(slip_triangle(1,:),slip_triangle(2,:),'DisplayName','slip','LineWidth',2,'Color',slip_color(w,:));

                    % plot drivetrain
                    % axles from axle center to wheel center
                    axle_center = ceil(w/2)+1;
                    axle = [drivetrain_I(:,axle_center) wheel_I(:,w)];
                    plot_dt(w) = plot(axle(1,:),axle(2,:),'--','DisplayName','axle','LineWidth',2.5,'Color',dt_color(w,:));
                end
                % plot motor and center axle
                plot_dt(5) = plot(drivetrain_I(1,1:2),drivetrain_I(2,1:2),'--','LineWidth',2.5,'Color',mean(dt_color(1:2,:)));
                plot_dt(6) = plot(drivetrain_I(1,1:2:3),drivetrain_I(2,1:2:3),'--','LineWidth',2.5,'Color',mean(dt_color(3:4,:)));
                plot_dt(7) = scatter(drivetrain_I(1,1),drivetrain_I(2,1),200*(L/diff(xlim)),mean(dt_color),'o','filled','HandleVisibility','off','LineWidth',4);
            else
                % update existing
                plot_car.XData = car_plot_pts(1,:);
                plot_car.YData = car_plot_pts(2,:);

                plot_cg(1).XData = car_I(1,1);
                plot_cg(1).YData = car_I(2,1);
                plot_cg(2).XData = car_I(1,1);
                plot_cg(2).YData = car_I(2,1);

                for(w = 1:4)
                    plot_wheel(w).XData = wheel_I(1,w+4:4:end);
                    plot_wheel(w).YData = wheel_I(2,w+4:4:end);
                    plot_wheel(w).LineWidth = wheel_width(w);
                    plot_wheel(w).Color = wheel_color(w,:);

                    % plot slip triangle
                    i_s = w*3-2;
                    slip_triangle = [slip_vis_I(:,i_s:i_s+2) slip_vis_I(:,i_s)];
                    plot_slip(w).XData = slip_triangle(1,:);
                    plot_slip(w).YData = slip_triangle(2,:);
                    plot_slip(w).Color = slip_color(w,:);

                    axle_center = ceil(w/2)+1;
                    axle = [drivetrain_I(:,axle_center) wheel_I(:,w)];
                    plot_dt(w).XData = axle(1,:);
                    plot_dt(w).YData = axle(2,:);
                    plot_dt(w).Color = dt_color(w,:);
                end
                plot_dt(5).XData = drivetrain_I(1,1:2);
                plot_dt(5).YData = drivetrain_I(2,1:2);
                plot_dt(5).Color = mean(dt_color(1:2,:));
                plot_dt(6).XData = drivetrain_I(1,1:2:3);
                plot_dt(6).YData = drivetrain_I(2,1:2:3);
                plot_dt(6).Color = mean(dt_color(3:4,:));

                plot_dt(7).XData = drivetrain_I(1,1);
                plot_dt(7).YData = drivetrain_I(2,1);
                plot_dt(7).CData = mean(dt_color);
            end

            % center frame on car
            fig_anim.Children.XLim = [-L,L] + X_hist(8,i);
            fig_anim.Children.YLim = 2*[-B,B] + X_hist(9,i);

            % save to gif
            frame = getframe(fig_anim);
            im{idx} = frame2im(frame);
            idx = idx+1;
            % pause(.001);
        end
    end
    keyboard

    % scatter plot of path, point every second
    num_pts = ((max(T)-min(T))/1);
    spacing = floor((numel(T)-1)/num_pts);
    % times chosen
    T_plot = T(1:spacing:end);
    % positions chosen
    pos_plot = X_hist(8:9,1:spacing:end);
    % headings chosen
    head_plot = [cos(X_hist(10,1:spacing:end));sin(X_hist(10,1:spacing:end))];

    % scatter plot open circles

    axis auto
    axis equal
    plot_path(1) = scatter(pos_plot(1,:),pos_plot(2,:),100,[92 125 196]/255,'o','LineWidth',3,'DisplayName','Path');
    % quiver of headings
    plot_path(2) = quiver(pos_plot(1,:),pos_plot(2,:),head_plot(1,:),head_plot(2,:),.5,'Color',[194 95 167]/255,'LineWidth',1.5,'MaxHeadSize',.03,'DisplayName','Heading','ShowArrowHead','off');

    plots = struct( ...
        'plot_car',plot_car, ...
        'plot_cg',plot_cg, ...
        'plot_wheel',plot_wheel, ...
        'plot_drivetrain',plot_dt, ...
        'plot_slip',plot_slip, ...
        'plot_path',plot_path );



end

