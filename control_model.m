% 
% wheels are at fl, fr, rl, rr
% inputs:
% t - current time step
% x - current state:
    %   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
%   vehicle - struct containing vehicle properties
%       L - length of vehicle
%       B - wheel base
%       Rw - wheel radius
%       L1 - dist from front axle to cg
%       L2 - dist from rear axle to cg
%       m - mass of vehicle
%       Iz - MOI of vehicle
%       Iw - MOI of wheel
%       @mu_slip_curve is a function handle pointing to a function:
%       inputs: slip s
%       outputs: [mu, Cf, Cr]
%   control_input  - struct containing current time control input:
%                   Td - driving torque 4x1 vector applied to wheels
%                   Tb - braking torque 4x1 applied to wheels
%                   deltaf - front steer angle
%                   deltar - rear steer angle
%   dist - 2x1 vector of disturbance forces acting through cg at current time [Fx,Fy]';
% constants - struct containing constants not related to vehicle:
%   g
% outputs:
% xdot - derivative of state
% y - additional outputs
function [xdot, y] = four_wheel_model(t,x,vehicle,control_input,dist,constants)
    % state x is:
    %   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
    %       first four states are the rotational velocities of each wheel
    wfl = x(1); wfr = x(2); wrl = x(3); wrr = x(4);
    %       next three states are the velocities in each dof in the body frame
    Vx = x(5); Vy = x(6); omegaz = x(7);
    %       next three states are the positions and heading in the inertial frame
    Xt = x(8); Yt = x(9); Psit = x(10);

    B = vehicle.B; Rw = vehicle.Rw; L1 = vehicle.L1; L2 = vehicle.L2;
    m = vehicle.m; Iz = vehicle.Iz; Iw = vehicle.Iw; g = constants.g;
    mu_slip_curve = vehicle.mu_slip_curve;

    deltaf = control_input.deltaf;
    deltar = control_input.deltar;
    Td = control_input.Td;
    Tb = control_input.Tb;

    % all wheels have the same velocity in the body frame, that of the vehicle (Vx,Vy) but will depend on steer angle when computing slip

    %%
    % compute slip angle
    alphafl = deltaf - atan2(Vy+L1*omegaz,Vx);
    alphafr = deltaf - atan2(Vy+L1*omegaz,Vx);
    alpharl = deltar - atan2(Vy-L2*omegaz,Vx);
    alpharr = deltar - atan2(Vy-L2*omegaz,Vx);
    
    % min velocity for slip?
    % compute slip at each wheel
    sfl = (Rw*wfl - Vx*cos(deltaf))/max(Rw*wfl,Vx*cos(deltaf));
    sfr = (Rw*wfr - Vx*cos(deltaf))/max(Rw*wfr,Vx*cos(deltaf));
    srl = (Rw*wrl - Vx*cos(deltar))/max(Rw*wrl,Vx*cos(deltar));
    srr = (Rw*wrr - Vx*cos(deltar))/max(Rw*wrr,Vx*cos(deltar));
    % if slip is non-existent (at rest) set to small value of slip
    slip_nan_val = .001;
    if(isnan(sfl))
        sfl = slip_nan_val;
    end
    if(isnan(sfr))
        sfr = slip_nan_val;
    end
    if(isnan(srl))
        srl = slip_nan_val;
    end
    if(isnan(srr))
        srr = slip_nan_val;
    end
    if(abs(sfl)>1)
        sfl = sign(sfl);
    end
    if(abs(sfr)>1)
        sfr = sign(sfr);
    end
    if(abs(srl)>1)
        srl = sign(srl);
    end
    if(abs(srr)>1)
        srr = sign(srr);
    end


    % compute mu and cornering stiffness for each wheel
    [mufl,Cfl] = mu_slip_curve(sfl);
    [mufr,Cfr] = mu_slip_curve(sfr);
    [murl,~,Crl] = mu_slip_curve(srl);
    [murr,~,Crr] = mu_slip_curve(srr);

    % compute lateral forces from cornering stiffness and slip angles
    Fyfl = Cfl*alphafl;
    Fyfr = Cfr*alphafr;
    Fyrl = Crl*alpharl;
    Fyrr = Crr*alpharr;

    % compute traction forces from mu and normal forces
    Fxfl = sign(sfl)*mufl*m*g*( L1/(L1+L2) ) * .5;
    Fxfr = sign(sfr)*mufr*m*g*( L1/(L1+L2) ) * .5;
    Fxrl = sign(srl)*murl*m*g*( L2/(L1+L2) ) * .5;
    Fxrr = sign(srr)*murr*m*g*( L2/(L1+L2) ) * .5;
    % Fxfl = 0;
    % Fxfr = 0;
    % Fxrl = 0;
    % Fxrr = 0;

    % compute total torques acting on each wheel
    % driving_torque - tractive_torque - braking_torque
    torquefl = Td(1) - Fxfl*Rw - sign(wfl)*Tb(1);
    torquefr = Td(2) - Fxfr*Rw - sign(wfr)*Tb(2);
    torquerl = Td(3) - Fxrl*Rw - sign(wrl)*Tb(3);
    torquerr = Td(4) - Fxrr*Rw - sign(wrr)*Tb(4);
    
    % compute total forces acting on cg
    Fxtot = (Fxfl + Fxfr)*cos(deltaf) + (Fxrl + Fxrr)*cos(deltar) + ...
            -(Fyfl + Fyfr)*sin(deltaf) - (Fyrl + Fyrr)*sin(deltar) + dist(1);
    Fytot = (Fyfl + Fyfr)*cos(deltaf) + (Fyrl + Fyrr)*cos(deltar) + ...
            (Fxfl + Fxfr)*sin(deltaf) + (Fxrl + Fxrr)*sin(deltar) + dist(2);

    % compute total moments acting on cg
    momentF = ( (Fyfl+Fyfr)*cos(deltaf) + (Fxfl+Fxfr)*sin(deltaf) ) * L1 + ...
              ( (Fxfr-Fxfl)*cos(deltaf) + (Fyfl-Fyfr)*sin(deltaf) ) * B/2;
    momentR = ( (Fyrl+Fyrr)*cos(deltar) + (Fxrl+Fxrr)*sin(deltar) ) * -L1 + ...
              ( (Fxrr-Fxrl)*cos(deltar) + (Fyrl-Fyrr)*sin(deltar) ) * -B/2;
    momentcg = momentF+momentR;

    % gather system equations
    wfl_dot = torquefl/Iw;
    wfr_dot = torquefr/Iw;
    wrl_dot = torquerl/Iw;
    wrr_dot = torquerr/Iw;

    Vx_dot = (m*Vy*omegaz + Fxtot)/m;
    Vy_dot = (-m*Vx*omegaz + Fytot)/m;
    % Vx_dot = ( Fxtot)/m;
    % Vy_dot = ( Fytot)/m;
    omegaz_dot = momentcg/Iz;

    Xt_dot = Vx*cos(Psit)-Vy*sin(Psit);
    Yt_dot = Vx*sin(Psit)+Vy*cos(Psit);
    Psit_dot = omegaz;

    % gather system derivative
    xdot = [wfl_dot, wfr_dot, wrl_dot, wrr_dot, ...
            Vx_dot, Vy_dot, omegaz_dot, ...
            Xt_dot, Yt_dot, Psit_dot]';

    % output variables
    y = [sfl, sfr, srl, srr, ...
         alphafl, alphafr, alpharl, alpharr, ...
         Fxfl, Fxfr, Fxrl, Fxrr, ...
         Fyfl, Fyfr, Fyrl, Fyrr, ...
         mufl, mufr, murl, murr]';

end
