% PID cruise control to keep a desired speed
% inputs:
%   X_c - current controller state variables
%       Ie - integral of error
%       z  - error value passed through a high pass filter
%   Y_c - current controller state variables output 
%       e_V - previous error in speed
%       z_dot - approximation of error derivative
%   X_v - current vehicle state
%   control_params - struct containing
%   V_d - desired velocity
%
%
% outputs:
%   T_com - commanded driving torque
%   Tb - braking torque
%   e_V - error in speed
function [T_com,Tb,e_V] = cruise_control(X_c,Y_c,X_v,control_params)

    V_des = control_params.V_des;
    Vx = X_v(5); Vy = X_v(6);
    % current total speed
    V = sqrt(Vx^2 + Vy^2);
    
    % not using the value in Y_c because V_des can be changed between the calls
    e_V = V_des - V;
    Ie  =  X_c(1);
    de  =  Y_c(2);

    % controller gains % careful to pick gains that do not cause the wheels to slip
    % gains are relatively optimized to not result in negative stiffness (does not go past limit on mu-slip curve)
    % speed difference ~2 m/s
    % numbers are from tuning a 'step response' 
   %  K  = 10;
    K  = 9;
    % Ki = 5;
    Ki = 5.5;
    % Kd = .15; 
    Kd = .2;
    Kol = 150;
    % total commanded torque
    T_c = Kol*(K*e_V + Ki*Ie + Kd*de);
    
    if(T_c > 0) % positive means apply a drive torque
        % rear wheel drive, split desired torque to both wheels
        Td = [T_c/2,T_c/2,0,0]';
        % saturate Td to prevent slip
        T_com = min(T_c,400*4);
        Tb = zeros(4,1);
    else % need to slow down, apply a braking torque
        % apply brakes on all 4 wheels evenly; could be replaced with a brake-bias calculation, etc
        Tb = -T_c/4*ones(4,1);
        T_com = 0;
        % Td = zeros(4,1);
    end
end
