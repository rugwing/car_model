% testing script for vehicle model
%% startup
clear
vehicle_script % generate vehicle parameters
% state x is:
%   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
%       first four states are the rotational velocities of each wheel

%% initial condition
V0=0; % m/s;
% calculate free rolling wheel velocities
w_w = V0/vehicle.Rw;
X0 = [w_w,w_w,w_w,w_w,...
    V0,0,0,...
    0,0,0,...
    zeros(4,1)', zeros(4,1)']'; % initial state at rest
% dummy variables if not assigned
Td = [0,0,0,0]';
Tb = [0,0,0,0]';
deltaf = -.0;
deltar = 0;
control_input = struct('Td',Td , 'Tb',Tb , 'deltaf',deltaf ,'deltar',deltar);
dist = [0,0]';

control_params = struct('V_des',0,'tau_d',1/(2*pi*10)); % struct containing control parameters
X_c0 = zeros(2,1); % assume zero starting integral and derivative error

%% simulation parameters
T0 = 0; Tf = 50;
h_c = .05; % control time step
N = 3; % inner loop time magnification
T = T0:h_c/N:Tf;

%% simulation packaging
params = struct('vehicle',vehicle,'control_input',control_input,'dist',dist,'constants',constants,'control_params',control_params);

% allocate histories
[X,Y] = ode(0,X0,params);
X = zeros(numel(X),numel(T));
Y = zeros(numel(Y),numel(T));
X_c = zeros(2,numel(T)); % addition of Id and z state variables from PID cruise control
Y_c = zeros(2,numel(T)); 
% histories related to the outer control loop
T_c = T0:h_c:Tf-h_c;
control_input_hist = struct('T_c',T_c,'Td',zeros(4,numel(T_c)),'Tb',zeros(4,numel(T_c)),'deltaf',zeros(1,numel(T_c)),'deltar',zeros(1,numel(T_c)));
dist_hist = struct('T_c',control_input_hist.T_c,'dist',zeros(2,numel(T_c)));
control_params_hist = struct('T_c',control_input_hist.T_c,'V_des',nan*zeros(1,numel(T_c)));

%% simulation loop
% include control states in total state 
X0_tot = [X0;X_c0];
i = 1; % state index
index_c = 1; % control history index
for(t = T_c)
    if(t > 0 && t < 2.5) % approx 4 m/s after 2.5 s
        control_input.Td = [0,0,300,300]'; % rear wheel drive
    else
        control_input.Td = [0,0,0,0]'; % no drive
    end
    if(t > 15) % braking period
        % control_input.Tb = [0,0,800,800]'; % rear brakes
        % control_input.Tb = [300,300,0,0]'; % front brakes
    else
        control_input.Tb = zeros(4,1);
    end

    if(t>  3-h_c && t < 3+h_c) % set cruise control at time 3 sec
        % when changing desired speed, should reset integral and error values
        control_params.V_des = 5;
        X_c0(1) = 0; % reset integral error
        Y_c0(1) = control_params.V_des-sqrt(X0(5)^2+X0(6)^2); % reset error
        X0_tot(end-1:end) = X_c0;
        Y_c0(2) = 0; % reset derivative error
    end
    if(t > 3 )
        % control_params.V_des = control_params.V_des + .02; % increasing speed
        % start cruise control 
        [Td,Tb,eV] = cruise_control(X_c0,Y_c0,X0,control_params);
        control_input.Td = Td;
        control_input.Tb = Tb;
        % dist = [-.1*m*g,0]'; % ex grade
        % dist = [0,100]';
    end
    % disturbance, 
    if(t > 10 && t < 30)
        % control_input.deltaf = .05;
        dist = [-.2*m*g,0]'; % ex grade
    else
        control_input.deltaf = .00;
        % dist = [0,0]';
    end

    params.control_input = control_input;
    params.dist = dist;
    params.control_params = control_params;

    %% simulation
    % run iteration on inner loop time magnification
    % [X_tot_step,T_step,X_dot_step,Y_step] = rk4_outputs(@vehicle_wrapper,X0_tot,params,t,t+h_c,h_c/N);
    [X_tot_step,T_step,X_tot_dot_step,Y_tot_step] = rk4_outputs(@vehicle_wrapper,X0_tot,params,t,t+h_c,h_c/N);

    % unpack control states from vehicle states
    X_step = X_tot_step(1:end-2,:);
    X_c_step = X_tot_step(end-1:end,:);
    X_dot_step = X_tot_dot_step(1:end-2,:);
    X_c_dot_step = X_tot_dot_step(end-1:end,:);
    Y_step = Y_tot_step(1:end-2,:);
    Y_c_step = Y_tot_step(end-1:end,:);


    %% storage
    % store control and dist history
    control_input_hist.Td(:,index_c) = control_input.Td;
    control_input_hist.Tb(:,index_c) = control_input.Tb;
    control_input_hist.deltaf(:,index_c) = control_input.deltaf;
    control_input_hist.deltar(:,index_c) = control_input.deltar;
    dist_hist.dist(:,index_c) = dist;
    control_params_hist.V_des(:,index_c) = control_params.V_des;

    % store state and output variable history
    X(:,i:i-1+size(X_step,2)) = X_step;
    Y(:,i:i-1+size(Y_step,2)) = Y_step;
    T(i:i-1+numel(T_step)) = T_step;
    X_c(:,i:i-1+size(X_c_step,2)) = X_c_step;
    Y_c(:,i:i-1+size(Y_c_step,2)) = Y_c_step;

    %% update
    i = i + numel(T_step)-1;
    index_c = index_c + 1;
    X0 = X_tot_step(1:end-2,end);
    X_c0 = X_tot_step(end-1:end,end);
    Y_c0 = Y_c_step(:,end);
    X0_tot = X_tot_step(:,end);
    % X0_tot = [X0;X_c0];
end

%% plots
fig_wheel_velocities = figure(1);
clf;
subplot(2,2,1);
title('front left wheel');
hold on;
plotHelper(T,Rw*X(1,:),'s','R_w*\omega_{fl}',0);
plotHelper(T,X(5,:),'d','Vx',0);
grid on; legend show;
subplot(2,2,2);
title('front right wheel');
plotHelper(T,Rw*X(2,:),'o','R_w*\omega_{fr}',0);
plotHelper(T,X(5,:),'d','Vx',0);
grid on; legend show;
subplot(2,2,3);
title('rear left wheel');
plotHelper(T,Rw*X(3,:),'o','R_w*\omega_{rl}',0);
plotHelper(T,X(5,:),'d','Vx',0);
grid on; legend show;
subplot(2,2,4);
title('rear right wheel');
plotHelper(T,Rw*X(4,:),'o','R_w*\omega_{rr}',0);
plotHelper(T,X(5,:),'d','Vx',0);
grid on; legend show;


fig_body_velocities = figure(2);
clf;
title('body velocities')
grid on;
hold on;
plotHelper(T,X(5,:),'o','V_x',5);
plotHelper(T,X(6,:),'sq','V_y',5);
plotHelper(T,X(7,:),'*','omega_z',5);
legend show;

fig_inertial_pos = figure(3);
clf;
subplot(2,1,1);
title('inertial position')
grid on;
hold on;
plotHelper(T,X(8,:),'o','X',5);
plotHelper(T,X(9,:),'sq','Y',5);
plotHelper(T,X(10,:),'*','Psi',5);
legend show;
subplot(2,1,2);
grid on; hold on;
title('psi')
plotHelper(T,X(10,:),'*','Psi',5);


fig_path = figure(4);
clf;
title('path');
grid on;
hold on;
plotHelper(X(8,:),X(9,:),'o','Path',20);
axis equal;

fig_slip = figure(5);
clf;
subplot(2,1,1);
title('longitudinal slip');
grid on;
hold on;
plotHelper(T,Y(1,:),'sq','sfl',5);
plotHelper(T,Y(2,:),'o','sfr',6);
plotHelper(T,Y(3,:),'*','srl',5);
plotHelper(T,Y(4,:),'+','srr',6);
legend show;

% fig_ang = figure(6);
% clf;
subplot(2,1,2);
title('slip angle');
grid on;
hold on;
plotHelper(T,Y(5,:),'sq','alphafl',5);
plotHelper(T,Y(6,:),'o','alphafr',6);
plotHelper(T,Y(7,:),'*','alpharl',5);
plotHelper(T,Y(8,:),'+','alpharr',6);
legend show;
ylim_low = mean(quantile(Y(5:8,:)',.25));
ylim_high = mean(quantile(Y(5:8,:)',.75));
if(ylim_high - ylim_low > .01 )
    ylim([ylim_low,ylim_high]);
end

fig_mu = figure(7);
clf;
title('mu');
grid on;
hold on;
plotHelper(T,Y(17,:),'sq','mufl',5);
plotHelper(T,Y(18,:),'o','mufr',6);
plotHelper(T,Y(19,:),'*','murl',5);
plotHelper(T,Y(20,:),'+','murr',6);
legend show;

fig_mu_slip = figure(8);
clf;
title('mu slip curve');
grid on;
hold on;
s_test = -1:.05:1;
mu_test = s_test;
for(i=1:numel(s_test))
    mu_test(i) = vehicle.mu_slip_curve(s_test(i));
end
plot(s_test,mu_test,'LineWidth',2.5,'DisplayName','mu-slip');
scatter(Y(1,1:N:end),Y(17,1:N:end),50,'sq','LineWidth',2,'MarkerEdgeColor',[0 .447 .741],'DisplayName','mufl');
scatter(Y(2,1:N:end),Y(18,1:N:end),50,'o','LineWidth',2,'MarkerEdgeColor',[.929 .694 .125],'DisplayName','mufr');
scatter(Y(3,1:N:end),Y(19,1:N:end),50,'*','LineWidth',2,'MarkerEdgeColor',[.466 .674 .188],'DisplayName','murl');
scatter(Y(4,1:N:end),Y(20,1:N:end),50,'+','LineWidth',2,'MarkerEdgeColor',[.635 .078 .184],'DisplayName','murr');
legend show;

fig_Fx = figure(9);
clf;
subplot(2,1,1);
title('longitudinal force');
grid on;
hold on;
plotHelper(T,Y(9,:),'sq','Fxfl',5);
plotHelper(T,Y(10,:),'o','Fxfr',6);
plotHelper(T,Y(11,:),'*','Fxrl',5);
plotHelper(T,Y(12,:),'+','Fxrr',6);
legend show;
ylim_low = mean(quantile(Y(9:12,:)',.25));
ylim_high = mean(quantile(Y(9:12,:)',.75));
if(ylim_high - ylim_low > 500)
    ylim([ylim_low,ylim_high]);
end
% fig_Fy = figure(10);
% clf;
subplot(2,1,2);
title('lateral force');
grid on;
hold on;
plotHelper(T,Y(13,:),'sq','Fyfl',5);
plotHelper(T,Y(14,:),'o','Fyfr',6);
plotHelper(T,Y(15,:),'*','Fyrl',5);
plotHelper(T,Y(16,:),'+','Fyrr',6);
legend show;
ylim_low = mean(quantile(Y(13:16,:)',.25));
ylim_high = mean(quantile(Y(13:16,:)',.75));
if(ylim_high - ylim_low > 100)
    ylim([ylim_low,ylim_high]);
end

fig_ctrl = figure(11);
clf;
subplot(4,1,1);
title('desired driving torque');
grid on; hold on;
plotHelper(control_input_hist.T_c,control_input_hist.Td(1,:),'s','des Td_{fl}',20);
plotHelper(T,X(11,:),'s','Td_{fl}',10);
plotHelper(control_input_hist.T_c,control_input_hist.Td(2,:),'o','des Td_{fr}',25);
plotHelper(T,X(12,:),'s','Td_{fr}',12);
plotHelper(control_input_hist.T_c,control_input_hist.Td(3,:),'*','des Td_{rl}',20);
plotHelper(T,X(13,:),'s','Td_{rl}',10);
plotHelper(control_input_hist.T_c,control_input_hist.Td(4,:),'+','des Td_{rr}',25);
plotHelper(T,X(14,:),'s','Td_{rr}',12);
legend show;
subplot(4,1,2);
title('desired braking torque');
grid on; hold on;
plotHelper(control_input_hist.T_c,control_input_hist.Tb(1,:),'s','des Tb_{fl}',20);
plotHelper(T,X(15,:),'s','Tb_{fl}',10);
plotHelper(control_input_hist.T_c,control_input_hist.Tb(2,:),'o','des Tb_{fr}',25);
plotHelper(T,X(16,:),'s','Tb_{fr}',12);
plotHelper(control_input_hist.T_c,control_input_hist.Tb(3,:),'*','des Tb_{rl}',20);
plotHelper(T,X(17,:),'s','Tb_{rl}',10);
plotHelper(control_input_hist.T_c,control_input_hist.Tb(4,:),'+','des Tb_{rr}',25);
plotHelper(T,X(18,:),'s','Tb_{rr}',12);
legend show;
subplot(4,1,3);
title('Steering angle');
grid on; hold on;
plotHelper(control_input_hist.T_c,control_input_hist.deltaf,'sq','Tb_{fl}',20);
plotHelper(control_input_hist.T_c,control_input_hist.deltar,'*','Tb_{fr}',25);
legend show;
subplot(4,1,4);
title('disturbances');
grid on; hold on;
plotHelper(dist_hist.T_c,dist_hist.dist(1,:),'sq','F_x',20);
plotHelper(dist_hist.T_c,dist_hist.dist(2,:),'*','F_y',25);
legend show;

fig_err = figure(12);
clf;
subplot(3,1,1)
title('speed error');
grid on; hold on;
% plotHelper(control_params_hist.T_c,control_params_hist.V_des,'.','des',20)
plotHelper(T,Y_c(1,:),'.','error',20);
plotHelper(T,X_c(2,:),'o','z',25);
legend show;
subplot(3,1,2)
title('derivative error');
plotHelper(T,Y_c(2,:),'.','d_e',20);
subplot(3,1,3)
title('integral error');
plotHelper(T,X_c(1,:),'.','d_e',20);



%% ode wrapper
function [xdot,y] = vehicle_wrapper(t,x,params)
    % params is struct containing vehicle, control_input,dist, and constants
    vehicle = params.vehicle;
    control_input = params.control_input;
    control_params = params.control_params;
    dist = params.dist;
    constants = params.constants;

    % unpack vehicle states and control states
    x_v = x(1:end-2);
    x_c = x(end-1:end);
    [xdot_v,y_v] = four_wheel_model(t,x_v,vehicle,control_input,dist,constants);
    [xdot_c,y_c] = cruise_control_model(t,x_c,x_v,vehicle,control_params);

    % combine states and outputs
    xdot = [xdot_v;xdot_c];
    y = [y_v;y_c];
end

% ode without controller states
function [xdot,y] = ode(t,x,params)
    % params is struct containing vehicle, control_input,dist, and constants
    vehicle = params.vehicle;
    control_input = params.control_input;
    control_params = params.control_params;
    dist = params.dist;
    constants = params.constants;

    [xdot,y] = four_wheel_model(t,x,vehicle,control_input,dist,constants);
end
