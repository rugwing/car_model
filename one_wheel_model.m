% single wheel model, using fl wheel as example
function [xdot, y] = four_wheel_model(t,x,vehicle,control_input,dist,constants)
    % state x is:
    %   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
    %       first four states are the rotational velocities of each wheel
    wfl = x(1); 
    Vx = x(2); 
    Xt = x(3); 
    % additional states are the drive and brake torques
    Td = x(4);
    Tb = x(5);

    L = vehicle.L; fr = vehicle.fr;
    B = vehicle.B; Rw = vehicle.Rw; L1 = vehicle.L1; L2 = vehicle.L2;
    m = vehicle.m; Iz = vehicle.Iz; Iw = vehicle.Iw; 
    g = constants.g;
    mu_slip_curve = vehicle.mu_slip_curve;
    drive_model = vehicle.drive_model;
    brake_model = vehicle.brake_model;

    % deltaf = control_input.deltaf;
    % deltar = control_input.deltar;
    % control input torques become desired torques to control to
    Td_des = control_input.Td;
    Tb_des = control_input.Tb;

    % if moving slow enough, apply a slight braking torque to stop the wheels from drifting
    V_min = .01; % limit to velocities

    % all wheels have the same velocity in the body frame, that of the vehicle (Vx,Vy) but will depend on steer angle when computing slip

    %%
    % compute slip angle
    % alphafl = deltaf - atan2(Vy+L1*omegaz,Vx);

    % min velocity for slip?
    % compute slip at each wheel
    % sfl = (Rw*wfl - Vx*cos(deltaf))/max(Rw*wfl,Vx*cos(deltaf));
    sfl = (Rw*wfl - Vx)/max(Rw*wfl,Vx);
    slip_nan_val = .000;
    if(isnan(sfl) || (abs(Vx < V_min) && (abs(Rw*wfl) < V_min)))
        sfl = slip_nan_val;
    end
    if(abs(sfl)>1)
        sfl = sign(sfl);
    end


    % compute mu and cornering stiffness for each wheel
    [mufl,Cfl] = mu_slip_curve(sfl);

    % maintain traction if slow speed
    if(sqrt(Vx^2) < V_min)
        [~,Cfmax,Crmax] = mu_slip_curve(0);
        Cfl = Cfmax; Cfr = Cfmax;
        Crl = Crmax; Crl = Crmax;
    end

    % compute lateral forces from cornering stiffness and slip angles
    % Fyfl = Cfl*alphafl;

    % if(sqrt(Vx^2 + Vy^2) < V_min)
    %     Fyfl = 0; Fyfr = 0;
    %     Fyrl = 0; Fyrr = 0;
    % end

    % compute traction forces from mu and normal forces
    Fxfl = sign(sfl)*mufl*m*g*( L1/(L1+L2) ) * .5;
    % rolling resistance
    Fr = 1*wfl;
    % Fr = 0;

    % if(Fxfl < (Td-Tb)/Rw) % wheel is torqued slower than driven, ie no spin
    %     Fxfl = (Td-Tb)/Rw;
    %     torquefl = Td
    % else
    %     torquefl = Td(1) - Fxfl*Rw - sign(wfl)*Tb(1) - Fr;
    %     keyboard
    % end

    % if moving slow enough, say wheel does not slip in traction calculations
    % if(abs(Vx) < V_min)
    %     Fxfl = (Td-Tb)/Rw;
    % end

    torquefl = Td(1) - Fxfl*Rw - sign(wfl)*Tb(1) ;
    % negative torque causes wheel to jump back and forth at low speed, shouldnt happen anyways
    if(wfl < .001 && torquefl < 0)
        % wheel lock up, so no net torque due to slip and slip is +-1
        torquefl = max(torquefl,0);
        sfl = sign(Rw*wfl-Vx);
        % torquefl = -wfl*20;
        % wfl_dot = -wfl*2/V_min ;
    end
    % compute total forces acting on cg
    Fxtot = Fxfl + dist(1) - Fr;
    % compute total moments acting on cg

    % gather system equations
    wfl_dot = torquefl/Iw;

    % Vx_dot = (m*Vy*omegaz + Fxtot)/m;
    Vx_dot = (Fxtot)/m;

    Xt_dot = Vx;
    % Xt_dot = Vx*cos(Psit)-Vy*sin(Psit);
    % Yt_dot = Vx*sin(Psit)+Vy*cos(Psit);
    % Psit_dot = omegaz;

    % motor and brake derivatives
    Td_dot = drive_model(Td,Td_des);
    Tb_dot = brake_model(Tb,Tb_des);

    % if(t==1.5);keyboard;end
    if(Vx < V_min) % assume no slip at slow speeds
        % no slip assumption; first term ensures that Vx goes to zero
        if(abs(Td) > 1e-4)
            Vx_dot = -Vx/V_min + (m + Iw/Rw^2)^-1 * (1/Rw*( max(0,Td-Tb) ));
        else
            % additional case if not driving, then velocity and omega should go down to zero
            Vx_dot = -Vx/V_min;
            wfl_dot = -wfl*2/V_min ;
        end
    end

    % gather system derivative
    xdot = [wfl_dot, ...
        Vx_dot, ...
        Xt_dot, Td_dot', Tb_dot']';
    % output variables
    y = [sfl, ...
        Fxfl, ...
        mufl, ...
        Fxtot, ...
        torquefl, ...
        Vx_dot, ...
        wfl_dot ...
        ]';
end
