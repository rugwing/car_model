% testing script for vehicle model
%% startup
clear
disp('begin setup procedure');
tic; % time startup procedure
vehicle_script % generate vehicle parameters
plot_flag = false;
anim_flag = true;
% state x is:
%   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
%       first four states are the rotational velocities of each wheel

%% initial condition
V0 = 0; % m/s;
% calculate free rolling wheel velocities
w_w = V0/vehicle.Rw;
X0 = [w_w,w_w,w_w,w_w,...
    V0,0,0,...
    0,0,0,...
    zeros(4,1)', zeros(4,1)']'; % initial state at rest
% dummy variables if not assigned
Td = [0,0,0,0]';
Tb = [0,0,0,0]';
deltaf = -.0;
deltar = 0;
abs_flag = zeros(4,1);
control_input = struct('Td',Td , 'Tb',Tb , 'deltaf',deltaf ,'deltar',deltar,'abs_flag',abs_flag);
dist = [0,0,0]';
control_params = struct('V_des',nan,'tau_d',1/(2*pi*10)); % struct containing control parameters
X_c0 = nan*zeros(2,1); % assume zero starting integral and derivative error

% calculate Rfactor for rocard model stability
[~,CCf,CCr] = vehicle.mu_slip_curve(0);
Cr = CCr*Wr/2;
Cf = CCf*Wf/2;
Rfactor = Cr*vehicle.L2 - Cf*vehicle.L1;
if(Rfactor<0)
    Vc = sqrt(2*Cf*Cr*vehicle.L^2 / ( vehicle.m *(-Rfactor)));
    if(V0 > Vc)
        disp(['Warning: vehicle is unstable based on Rocard Stability. Too fast by ' num2str(V0-Vc) '. Reduce initial velocity']);
    elseif(abs(V0-Vc) < 5)
        % disp('Vehicle is close to critical velocity. Consider reducing initial velocity');
    end
else
    % disp('No critical velocity based on Rocard Stability');
end
%% set braking case
case_brake = 4;
switch case_brake
    case 1
        Tb_case = [800,800,800,800]';
    case 2
        Tb_case = [0,0,800,800]';
    case 3
        Tb_case = [800,800,0,0]';
    case 4
        Tb_case = [0,800,800,0]';
    case 5
        Tb_case = [0,800,800,800]';
    case 6
        Tb_case = [800,800,0,800]';
    otherwise
        Tb_case = [0,0,0,0]';
end
%% simulation parameters
T0 = -5; Tf = 40;
h_c = .05; % control time step
N = 5; % inner loop time magnification
T = T0:h_c/N:Tf;

%% simulation packaging
params = struct('vehicle',vehicle,'control_input',control_input,'dist',dist,'constants',constants,'control_params',control_params);

% allocate histories
[X,Y] = ode(0,X0,params);
Y0 = Y;
X = zeros(numel(X),numel(T));
Y = zeros(numel(Y),numel(T));
X_c = zeros(2,numel(T)); % addition of Id and z state variables from PID cruise control
Y_c = zeros(2,numel(T)); 
Y_c0 = [0,0]';
% histories related to the outer control loop
T_c = T0:h_c:Tf-h_c;
control_input_hist = struct('T_c',T_c,'T_com',zeros(1,numel(T_c)),'Td',zeros(4,numel(T_c)),'Tb',zeros(4,numel(T_c)),'deltaf',zeros(1,numel(T_c)),'deltar',zeros(1,numel(T_c)),'abs_flag',zeros(4,numel(T_c)));
dist_hist = struct('T_c',control_input_hist.T_c,'dist',zeros(3,numel(T_c)));
control_params_hist = struct('T_c',control_input_hist.T_c,'V_des',nan*zeros(1,numel(T_c)));

% combine initial states
X0_tot = [X0;X_c0];
Y0_tot = [Y0;Y_c0];
toc;

%% simulation loop
disp('begin simulation loop');
tic;
i = 1; % state index
index_c = 1; % control history index
cruise_flag = 0; % begin without cruise control, should always be set this way
for(t = T_c)
    %% vehicle top level state
    % if(t<3.5)
    if(t<=4)
        % drive
        [Td_des,Tb,torque_com,abs_flag,X0_tot,Y0_tot,cruise_flag,control_params] = top_level_control('drive',[600],X0_tot,Y0_tot,vehicle,control_input,control_params);
    elseif( t > 4 && t <20)
        % cruise control
        [Td_des,Tb,torque_com,abs_flag,X0_tot,Y0_tot,cruise_flag,control_params] = top_level_control('cruise',[1,cruise_flag],X0_tot,Y0_tot,vehicle,control_input,control_params);
    elseif(t >= 20 )
        [Td_des,Tb,torque_com,abs_flag,X0_tot,Y0_tot,cruise_flag,control_params] = top_level_control('brake',Tb_case,X0_tot,Y0_tot,vehicle,control_input,control_params);
    else
        [Td_des,Tb,torque_com,abs_flag,X0_tot,Y0_tot,cruise_flag,control_params] = top_level_control('',[],X0_tot,Y0_tot,vehicle,control_input,control_params);
    end
    % assign to control
    control_input.Td = Td_des;
    control_input.T_com = torque_com;
    control_input.Tb = Tb;
    control_input.abs_flag = abs_flag;

    %% disturbance and steer
    % disturbance and steering
    if(t >= 10 && t < 12.5 )
        % 7% grade
        dist = [0,-00,0.1]';
        control_input.deltaf = .03;
    elseif(t >= 12.5 && t < 15 )
        control_input.deltaf = .0;
    elseif(t >= 15 && t < 17.5 )
        control_input.deltaf = -.03;
    elseif(t > 20 && t <30)
        dist = [0,100,0]';
        control_input.deltaf = .0;
    elseif(t >=30)
        dist = [0 10 0]';
        control_input.deltaf = .0;
    else
        dist = [0,0,0]';
        control_input.deltaf = .00;
    end

    params.control_input = control_input;
    params.dist = dist;
    params.control_params = control_params;

    %% simulation
    % run iteration on inner loop time magnification
    % [X_tot_step,T_step,X_dot_step,Y_step] = rk4_outputs(@vehicle_wrapper,X0_tot,params,t,t+h_c,h_c/N);
    [X_tot_step,T_step,X_tot_dot_step,Y_tot_step] = rk4_outputs(@vehicle_wrapper,X0_tot,params,t,t+h_c,h_c/N);

    % unpack control states from vehicle states
    X_step = X_tot_step(1:end-2,:);
    X_c_step = X_tot_step(end-1:end,:);
    X_dot_step = X_tot_dot_step(1:end-2,:);
    X_c_dot_step = X_tot_dot_step(end-1:end,:);
    Y_step = Y_tot_step(1:end-2,:);
    Y_c_step = Y_tot_step(end-1:end,:);


    %% storage
    % store control and dist history
    control_input_hist.Td(:,index_c) = control_input.Td;
    control_input_hist.Tb(:,index_c) = control_input.Tb;
    control_input_hist.deltaf(:,index_c) = control_input.deltaf;
    control_input_hist.deltar(:,index_c) = control_input.deltar;
    control_input_hist.abs_flag(:,index_c) = control_input.abs_flag;
    control_input_hist.T_com(:,index_c) = control_input.T_com;
    dist_hist.dist(:,index_c) = dist;
    control_params_hist.V_des(:,index_c) = control_params.V_des;

    % store state and output variable history
    X(:,i:i-1+size(X_step,2)) = X_step;
    Y(:,i:i-1+size(Y_step,2)) = Y_step;
    T(i:i-1+numel(T_step)) = T_step;
    X_c(:,i:i-1+size(X_c_step,2)) = X_c_step;
    Y_c(:,i:i-1+size(Y_c_step,2)) = Y_c_step;

    %% update
    i = i + numel(T_step)-1;
    index_c = index_c + 1;
    X0 = X_tot_step(1:end-2,end);
    X_c0 = X_tot_step(end-1:end,end);
    Y_c0 = Y_c_step(:,end);
    X0_tot = X_tot_step(:,end);
    Y0_tot = Y_tot_step(:,end);
    % X0_tot = [X0;X_c0];
 end
toc;
%% animation
if(anim_flag)
    tic;
    disp('begin animation');
    fig_anim = figure(1);clf;cla;
    plots_anim = animate_vehicle(fig_anim,T,X,Y,X_c,Y_c, T_c,dist_hist,control_params_hist,control_input_hist, vehicle);
    toc;
end

%% plotting
if(plot_flag)
    tic;
    disp('begin plotting');
    %% plots
    % fig_wheel_velocities = figure(1);
    % clf;
    % subplot(2,2,1);
    % title('front left wheel');
    % hold on;
    % plotHelper(T,Rw*X(1,:),'s','R_w*\omega_{fl}',0);
    % plotHelper(T,X(5,:),'d','Vx',0);
    % grid on; legend show;
    % subplot(2,2,2);
    % title('front right wheel');
    % plotHelper(T,Rw*X(2,:),'o','R_w*\omega_{fr}',0);
    % plotHelper(T,X(5,:),'d','Vx',0);
    % grid on; legend show;
    % subplot(2,2,3);
    % title('rear left wheel');
    % plotHelper(T,Rw*X(3,:),'o','R_w*\omega_{rl}',0);
    % plotHelper(T,X(5,:),'d','Vx',0);
    % grid on; legend show;
    % subplot(2,2,4);
    % title('rear right wheel');
    % plotHelper(T,Rw*X(4,:),'o','R_w*\omega_{rr}',0);
    % plotHelper(T,X(5,:),'d','Vx',0);
    % grid on; legend show;


    fig_body_velocities = figure(2);
    clf;
    subplot(2,1,1);
    title('body velocities');
    grid on;
    hold on;
    plotHelper(T,X(5,:),'o','V_x',5);
    plotHelper(T,X(6,:),'sq','V_y',5);
    plotHelper(T,X(7,:),'*','omega_z',5);
    plotHelper(control_params_hist.T_c,control_params_hist.V_des,'.','cruise control setpoint',20);
    legend show;

    subplot(2,1,2);
    title('body forces');
    grid on;
    hold on;
    plotHelper(T,Y(21,:),'o','F_y',5);
    plotHelper(T,Y(22,:),'sq','F_x',5);
    plotHelper(T,Y(23,:),'*','M_z',5);
    plotHelper(T,Y(24,:),'^','F_r',10);
    legend show;

    fig_inertial_pos = figure(3);
    clf;
    subplot(2,1,1);
    title('inertial position')
    grid on;
    hold on;
    plotHelper(T,X(8,:),'o','X',5);
    plotHelper(T,X(9,:),'sq','Y',5);
    plotHelper(T,X(10,:),'*','Psi',5);
    legend show;
    subplot(2,1,2);
    grid on; hold on;
    title('psi')
    plotHelper(T,X(10,:),'*','Psi',5);


    fig_path = figure(4);
    clf;
    title('path');
    grid on;
    hold on;
    plotHelper(X(8,:),X(9,:),'o','Path',20);
    axis equal;

    fig_slip = figure(5);
    clf;
    subplot(2,1,1);
    title('longitudinal slip');
    grid on;
    hold on;
    plotHelper(T,Y(1,:),'sq','sfl',5);
    plotHelper(T,Y(2,:),'o','sfr',6);
    plotHelper(T,Y(3,:),'*','srl',5);
    plotHelper(T,Y(4,:),'+','srr',6);
    legend show;

    % fig_ang = figure(6);
    % clf;
    subplot(2,1,2);
    title('slip angle');
    grid on;
    hold on;
    plotHelper(T,Y(5,:),'sq','alphafl',5);
    plotHelper(T,Y(6,:),'o','alphafr',6);
    plotHelper(T,Y(7,:),'*','alpharl',5);
    plotHelper(T,Y(8,:),'+','alpharr',6);
    legend show;
    ylim_low = mean(quantile(Y(5:8,:)',.25));
    ylim_high = mean(quantile(Y(5:8,:)',.75));
    if(ylim_high - ylim_low > .01 )
        ylim([ylim_low,ylim_high]);
    end

    fig_mu = figure(7);
    clf;
    title('mu');
    grid on;
    hold on;
    plotHelper(T,Y(17,:),'sq','mufl',5);
    plotHelper(T,Y(18,:),'o','mufr',6);
    plotHelper(T,Y(19,:),'*','murl',5);
    plotHelper(T,Y(20,:),'+','murr',6);
    legend show;

    fig_mu_slip = figure(8);
    clf;
    title('mu slip curve');
    grid on;
    hold on;
    s_test = -1:.05:1;
    mu_test = s_test;
    for(i=1:numel(s_test))
        mu_test(i) = vehicle.mu_slip_curve(s_test(i));
    end
    plot(s_test,mu_test,'LineWidth',2.5,'DisplayName','mu-slip');
    scatter(Y(1,1:N:end),Y(17,1:N:end),50,'sq','LineWidth',2,'MarkerEdgeColor',[0 .447 .741],'DisplayName','mufl');
    scatter(Y(2,1:N:end),Y(18,1:N:end),50,'o','LineWidth',2,'MarkerEdgeColor',[.929 .694 .125],'DisplayName','mufr');
    scatter(Y(3,1:N:end),Y(19,1:N:end),50,'*','LineWidth',2,'MarkerEdgeColor',[.466 .674 .188],'DisplayName','murl');
    scatter(Y(4,1:N:end),Y(20,1:N:end),50,'+','LineWidth',2,'MarkerEdgeColor',[.635 .078 .184],'DisplayName','murr');
    legend show;

    fig_Fx = figure(9);
    clf;
    subplot(2,1,1);
    title('longitudinal force');
    grid on;
    hold on;
    plotHelper(T,Y(9,:),'sq','Fxfl',5);
    plotHelper(T,Y(10,:),'o','Fxfr',6);
    plotHelper(T,Y(11,:),'*','Fxrl',5);
    plotHelper(T,Y(12,:),'+','Fxrr',6);
    legend show;
    ylim_low = mean(quantile(Y(9:12,:)',.25));
    ylim_high = mean(quantile(Y(9:12,:)',.75));
    if(ylim_high - ylim_low > 500)
        ylim([ylim_low,ylim_high]);
    end
    subplot(2,1,2);
    title('lateral force');
    grid on;
    hold on;
    plotHelper(T,Y(13,:),'sq','Fyfl',5);
    plotHelper(T,Y(14,:),'o','Fyfr',6);
    plotHelper(T,Y(15,:),'*','Fyrl',5);
    plotHelper(T,Y(16,:),'+','Fyrr',6);
    legend show;
    ylim_low = mean(quantile(Y(13:16,:)',.25));
    ylim_high = mean(quantile(Y(13:16,:)',.75));
    if(ylim_high - ylim_low > 100)
        ylim([ylim_low,ylim_high]);
    end

    fig_ctrl = figure(11);
    clf;
    subplot(4,1,1);
    title('desired driving torque');
    grid on; hold on;
    plotHelper(control_input_hist.T_c,control_input_hist.Td(1,:),'s','des Td_{fl}',20);
    plotHelper(T,X(11,:),'s','Td_{fl}',10);
    plotHelper(control_input_hist.T_c,control_input_hist.Td(2,:),'o','des Td_{fr}',25);
    plotHelper(T,X(12,:),'s','Td_{fr}',12);
    plotHelper(control_input_hist.T_c,control_input_hist.Td(3,:),'*','des Td_{rl}',20);
    plotHelper(T,X(13,:),'s','Td_{rl}',10);
    plotHelper(control_input_hist.T_c,control_input_hist.Td(4,:),'+','des Td_{rr}',25);
    plotHelper(T,X(14,:),'s','Td_{rr}',12);
    legend show;
    subplot(4,1,2);
    title('desired braking torque');
    grid on; hold on;
    plotHelper(control_input_hist.T_c,control_input_hist.Tb(1,:),'s','des Tb_{fl}',20);
    plotHelper(T,X(15,:),'s','Tb_{fl}',10);
    plotHelper(control_input_hist.T_c,control_input_hist.Tb(2,:),'o','des Tb_{fr}',25);
    plotHelper(T,X(16,:),'s','Tb_{fr}',12);
    plotHelper(control_input_hist.T_c,control_input_hist.Tb(3,:),'*','des Tb_{rl}',20);
    plotHelper(T,X(17,:),'s','Tb_{rl}',10);
    plotHelper(control_input_hist.T_c,control_input_hist.Tb(4,:),'+','des Tb_{rr}',25);
    plotHelper(T,X(18,:),'s','Tb_{rr}',12);
    legend show;
    subplot(4,1,3);
    title('Steering angle');
    grid on; hold on;
    plotHelper(control_input_hist.T_c,control_input_hist.deltaf,'sq','Tb_{fl}',20);
    plotHelper(control_input_hist.T_c,control_input_hist.deltar,'*','Tb_{fr}',25);
    legend show;
    subplot(4,1,4);
    title('disturbances');
    grid on; hold on;
    plotHelper(dist_hist.T_c,dist_hist.dist(1,:),'sq','F_x',20);
    plotHelper(dist_hist.T_c,dist_hist.dist(2,:),'*','F_y',25);
    legend show;

    fig_err = figure(12);
    clf;
    subplot(4,1,1)
    title('speed error');
    grid on; hold on;
    % plotHelper(control_params_hist.T_c,control_params_hist.V_des,'.','des',20);
    plotHelper(T,Y_c(1,:),'.','error',20);
    plotHelper(T,X_c(2,:),'o','z',25);
    legend show;
    subplot(4,1,2)
    grid on; hold on;
    title('derivative error');
    plotHelper(T,Y_c(2,:),'.','d_e',20);
    ylim_low = mean(quantile(Y_c(2,:)',.005));
    ylim_high = mean(quantile(Y_c(2,:)',.995));
    if(ylim_high - ylim_low > 1e-3)
        ylim([ylim_low,ylim_high]);
    end
    subplot(4,1,3)
    grid on; hold on;
    title('integral error');
    plotHelper(T,X_c(1,:),'.','d_e',20);
    subplot(4,1,4)
    grid on; hold on;
    title('torque commanded vs applied');
    plotHelper(T_c,control_input_hist.T_com,'o','command',20);
    plotHelper(T_c,sum(control_input_hist.Td),'sq','total applied',20);
    legend show;

    fig_abs = figure(13);
    clf;
    % title('abs')
    abs_color_on  = [235 86 45]/255;
    abs_color_off = [63 201 32]/255;
    abs_color_diff = abs_color_on-abs_color_off;
    abs_colors = zeros(numel(T_c),3,4);
    abs_colors_interp = zeros(numel(T),3,4);
    for(c = 1:4)
        abs_colors(:,:,c) = abs_color_off + control_input_hist.abs_flag(c,:)'.*abs_color_diff;
        abs_colors_interp(:,:,c) = interp1(1:numel(T_c),abs_colors(:,:,c),(1:numel(T))/N,'prev',0);
    end

    subplot(2,2,1);
    title('front left abs');
    hold on; grid on;
    scatter(T,Rw*X(1,:),25,abs_colors_interp(:,:,1),'s','filled','DisplayName','R_w*\omega_{fl}');
    plotHelper(T,X(5,:),'d','Vx',0);
    grid on; legend show;
    subplot(2,2,2);
    title('front right abs');
    hold on; grid on;
    scatter(T,Rw*X(2,:),25,abs_colors_interp(:,:,2),'s','filled','DisplayName','R_w*\omega_{fr}');
    plotHelper(T,X(5,:),'d','Vx',0);
    grid on; legend show;
    subplot(2,2,3);
    title('rear left abs');
    hold on; grid on;
    scatter(T,Rw*X(3,:),25,abs_colors_interp(:,:,3),'s','filled','DisplayName','R_w*\omega_{rl}');
    plotHelper(T,X(5,:),'d','Vx',0);
    grid on; legend show;
    subplot(2,2,4);
    title('rear right abs');
    hold on; grid on;
    scatter(T,Rw*X(4,:),25,abs_colors_interp(:,:,4),'s','filled','DisplayName','R_w*\omega_{rr}');
    plotHelper(T,X(5,:),'d','Vx',0);
    grid on; legend show;

    fig_w = figure(14);
    clf;
    hold on; grid on;
    title('weight transfer');
    plotHelper(T,Y(25,:),'s','fl weight transfer',10);
    plotHelper(T,Y(26,:),'o','fr weight transfer',15);
    plotHelper(T,Y(27,:),'*','rl weight transfer',10);
    plotHelper(T,Y(28,:),'+','rr weight transfer',15);
    plot(T,Wf/2*(T*0+1),'--','LineWidth',1.5,'DisplayName','static front weight');
    plot(T,Wr/2*(T*0+1),'--','LineWidth',1.5,'DisplayName','static rear weight');

    legend show;
    toc;
end

%% helper functions

% helper function to clean up control of the vehicle; could be extended to provide more autonomous control of vehicle
% state is one of: 'drive, brake_no_abs, brake, cruise'
% params is a column vector of parameters of that state
% state             - params
% 'drive'           - [torque_com] % commanded torque
% 'brake no abs'    - [Tb(4x1)] % braking torque
% 'brake'           - [Tb_com(4x1)] % braking torque
% 'cruise'          - [V_step, cruise_flag] % increase in velocity from current, cruise_flag is true if beginning cruise control
%
% state             - status
% 'drive'           - [] 
% 'brake no abs'    - []
% 'brake'           - []
% 'cruise'          - []
% last value of state will always be the cruise_flag
% outputs Td_des and Tb to be assigned to control_input
% X is the input state, adjusted if cruise control is turned on or off
% status is a col vector of additional outputs; eg abs_flag
function [Td_des,Tb,torque_com,abs_flag,X,Y,cruise_flag,control_params] = top_level_control(state,params,X,Y,vehicle,control_input,control_params)
    switch state
        case 'drive'
            % accelerate by pushing motor with desired torque
            torque_com = params(1);
            [Td_des] = vehicle.drivetrain_model(torque_com,X);
            Tb = zeros(4,1);
            status = [];
        case 'brake no abs'
            Td_des = zeros(4,1);
            torque_com = 0;
            Tb = params(1:4);
            status = [];
        case 'brake'
            torque_com = 0;
            Td_des = zeros(4,1);
            Tb_com = params(1:4);
            [Tb,abs_flag] = abs_control(X,Tb_com,vehicle,control_input);
            status = abs_flag;
        otherwise % no input coasting
            Td_des = zeros(4,1);
            Tb = zeros(4,1);
            torque_com = 0;
            abs_flag = zeros(4,1);
    end
    if(strcmp(state,'cruise')) % separate handling of cruise control to set X
        V_step = params(1);
        cruise_flag = params(2);
        if(cruise_flag == 0)
            % initialize cruise control
            control_params.V_des = X(5) + V_step;
            % reset error
            X_c = zeros(2,1);
            Y_c = zeros(2,1);
            X(end-1:end) = X_c;
            Y(end-1:end) = Y_c;

            cruise_flag = 1;
        end
        X_c = X(end-1:end); Y_c = Y(end-1:end);
        [torque_com,Tb,~] = cruise_control(X_c,Y_c,X,control_params);
        Td_des = vehicle.drivetrain_model(torque_com,X);
        params = torque_com;
        status = [];
    else
        % reset cruise control states and turn off control
        % params(end+1) = torque_com;
        X_c = nan*zeros(2,1); Y_c = X_c;
        X(end-1:end) = X_c;
        Y(end-1:end) = Y_c;
        control_params.V_des = nan;
        cruise_flag = 0;
    end
    if(~strcmp(state,'brake'))
        % if not under abs brake, turn off abs flag
        abs_flag = zeros(4,1);
    end
end
%% ode wrappers
% vehicle wrapper
function [xdot,y] = vehicle_wrapper(t,x,params)
    % params is struct containing vehicle, control_input,dist, and constants
    vehicle = params.vehicle;
    control_input = params.control_input;
    control_params = params.control_params;
    dist = params.dist;
    constants = params.constants;

    % unpack vehicle states and control states
    x_v = x(1:end-2);
    x_c = x(end-1:end);
    [xdot_v,y_v] = four_wheel_model(t,x_v,vehicle,control_input,dist,constants);
    [xdot_c,y_c] = cruise_control_model(t,x_c,x_v,vehicle,control_params);

    % combine states and outputs
    xdot = [xdot_v;xdot_c];
    y = [y_v;y_c];
end

% ode without controller states
function [xdot,y] = ode(t,x,params)
    % params is struct containing vehicle, control_input,dist, and constants
    vehicle = params.vehicle;
    control_input = params.control_input;
    control_params = params.control_params;
    dist = params.dist;
    constants = params.constants;

    [xdot,y] = four_wheel_model(t,x,vehicle,control_input,dist,constants);
end
