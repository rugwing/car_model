% state model for the control state variables
% inputs:
% t - current time step
% x_c - current control state:
%   x = [Ie, z]';
%       Ie - integral of speed error
%       z - approximate error used for derivative error approx
% x - current vehicle state:
%   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
%   vehicle - struct containing vehicle properties
%       L - length of vehicle
%       B - wheel base
%       Rw - wheel radius
%       L1 - dist from front axle to cg
%       L2 - dist from rear axle to cg
%       m - mass of vehicle
%       Iz - MOI of vehicle
%       Iw - MOI of wheel
%       @mu_slip_curve is a function handle pointing to a function:
%       inputs: slip s
%       outputs: [mu, Cf, Cr]
% control_params - struct containing parameters related to control (eg. speed setpoint)
%       V_des - desired speed
%
% outputs:
% xdot_c - derivative of control state
% y_c - additional outputs
function [xdot_c, y_c] = cruise_control_model(t,x_c,x,vehicle,control_params)
    % state x is:
    %   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
    %       first four states are the rotational velocities of each wheel
    wfl = x(1); wfr = x(2); wrl = x(3); wrr = x(4);
    %       next three states are the velocities in each dof in the body frame
    Vx = x(5); Vy = x(6); omegaz = x(7);
    %       next three states are the positions and heading in the inertial frame
    Xt = x(8); Yt = x(9); Psit = x(10);

    B = vehicle.B; Rw = vehicle.Rw; L1 = vehicle.L1; L2 = vehicle.L2;
    m = vehicle.m; Iz = vehicle.Iz; Iw = vehicle.Iw; 
    mu_slip_curve = vehicle.mu_slip_curve;

    %%     % min velocity for slip?
    %     %% compute slip at each wheel
    %     sfl = (Rw*wfl - Vx*cos(deltaf))/max(Rw*wfl,Vx*cos(deltaf));
    %     sfr = (Rw*wfr - Vx*cos(deltaf))/max(Rw*wfr,Vx*cos(deltaf));
    %     srl = (Rw*wrl - Vx*cos(deltar))/max(Rw*wrl,Vx*cos(deltar));
    %     srr = (Rw*wrr - Vx*cos(deltar))/max(Rw*wrr,Vx*cos(deltar));
    %     % if slip is non-existent (at rest) set to small value of slip
    %     slip_nan_val = .001;
    % 
    %     if(isnan(sfl))
    %         sfl = slip_nan_val;
    %     end
    %     if(isnan(sfr))
    %         sfr = slip_nan_val;
    %     end
    %     if(isnan(srl))
    %         srl = slip_nan_val;
    %     end
    %     if(isnan(srr))
    %         srr = slip_nan_val;
    %     end
    %     if(abs(sfl)>1)
    %         sfl = sign(sfl);
    %     end
    %     if(abs(sfr)>1)
    %         sfr = sign(sfr);
    %     end
    %     if(abs(srl)>1)
    %         srl = sign(srl);
    %     end
    %     if(abs(srr)>1)
    %         srr = sign(srr);
    %     end
    % 
    % control params
    V_des = control_params.V_des;
    tau_d = control_params.tau_d;
    % control states
    Ie = x_c(1);
    z  = x_c(2);
    % calculate speed error
    V_tot = sqrt(Vx^2 + Vy^2);
    e_V = V_des - V_tot;

    %% outputs
    % derivatives of control states
    Ie_dot = e_V;
    z_dot  = (-z + e_V)/tau_d; % z_dot is the approximation of the derivative of the error d_e


    % gather system derivative
    xdot_c = [Ie_dot,z_dot]';

    % output variables
    y_c = [e_V,z_dot]';

end
