% testing script for vehicle model
%% startup
clear
vehicle_script % generate vehicle parameters
plot_flag = true;
anim_flag = false;
% state x is:
%   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
%       first four states are the rotational velocities of each wheel

%% initial condition
V0=0; % m/s;
% calculate free rolling wheel velocities
w_w = V0/vehicle.Rw;
X0 = [w_w,...
    V0,...
    0,...
    zeros(1,1)', zeros(1,1)']'; % initial state at rest
% dummy variables if not assigned
Td = [0]';
Tb = [0]';
deltaf = -.0;
deltar = 0;
abs_flag = zeros(4,1);
control_input = struct('Td',Td , 'Tb',Tb , 'deltaf',deltaf ,'deltar',deltar,'abs_flag',abs_flag);
dist = [0,0]';

control_params = struct('V_des',V0,'tau_d',1/(2*pi*10)); % struct containing control parameters

%% simulation parameters
T0 = 0; Tf = 20;
h_c = .05; % control time step
N = 5; % inner loop time magnification
T = T0:h_c/N:Tf;

%% simulation packaging
params = struct('vehicle',vehicle,'control_input',control_input,'dist',dist,'constants',constants,'control_params',control_params);

% allocate histories
[X,Y] = ode(0,X0,params);
X = zeros(numel(X),numel(T));
Y = zeros(numel(Y),numel(T));
% histories related to the outer control loop
T_c = T0:h_c:Tf-h_c;
control_input_hist = struct('T_c',T_c,'Td',zeros(1,numel(T_c)),'Tb',zeros(1,numel(T_c)),'deltaf',zeros(1,numel(T_c)),'deltar',zeros(1,numel(T_c)),'abs_flag',zeros(4,numel(T_c)));
dist_hist = struct('T_c',control_input_hist.T_c,'dist',zeros(2,numel(T_c)));
control_params_hist = struct('T_c',control_input_hist.T_c,'V_des',nan*zeros(1,numel(T_c)));

turn_flag = 0;
%% simulation loop
% include control states in total state 
X0_tot = [X0;];
i = 1; % state index
index_c = 1; % control history index
for(t = T_c)
    % control_input.Td = 10;
    if(t >= 1 && t < 5) % approx 4 m/s after 2.5 s
        control_input.Td = [600]'; % rear wheel drive
        % control_input.Td = Td;
        % control_input.Tb = Tb;
    elseif(t > 16) % drive again
        control_input.Td = 500;
    else
        control_input.Td = [0]'; % no drive
    end
    if(t > 8 && t < 13)  % braking period
        control_input.Tb = [800]';
        % control_input.Tb = [0,0,800,800]'; % rear brakes
        % control_input.Tb = [1200,1200,0,0]'; % front brakes
        % apply abs control on Tb
        % [control_input.Tb,control_input.abs_flag] = abs_control(X0,control_input.Tb,vehicle,control_input);
    else
        control_input.Tb = zeros(1,1);
    end


    params.control_input = control_input;
    params.dist = dist;
    params.control_params = control_params;

    %% simulation
    % run iteration on inner loop time magnification
    % [X_tot_step,T_step,X_dot_step,Y_step] = rk4_outputs(@vehicle_wrapper,X0_tot,params,t,t+h_c,h_c/N);
    [X_tot_step,T_step,X_tot_dot_step,Y_tot_step] = rk4_outputs(@vehicle_wrapper,X0_tot,params,t,t+h_c,h_c/N);

    % unpack control states from vehicle states
    X_step = X_tot_step;
    X_dot_step = X_tot_dot_step;
    Y_step = Y_tot_step;

    %% storage
    % store control and dist history
    control_input_hist.Td(:,index_c) = control_input.Td;
    control_input_hist.Tb(:,index_c) = control_input.Tb;
    control_input_hist.deltaf(:,index_c) = control_input.deltaf;
    control_input_hist.deltar(:,index_c) = control_input.deltar;
    control_input_hist.abs_flag(:,index_c) = control_input.abs_flag;
    dist_hist.dist(:,index_c) = dist;
    control_params_hist.V_des(:,index_c) = control_params.V_des;

    % store state and output variable history
    X(:,i:i-1+size(X_step,2)) = X_step;
    Y(:,i:i-1+size(Y_step,2)) = Y_step;
    T(i:i-1+numel(T_step)) = T_step;

    %% update
    i = i + numel(T_step)-1;
    index_c = index_c + 1;
    X0 = X_tot_step(1:end-2,end);
    X0_tot = X_tot_step(:,end);
    % X0_tot = [X0;X_c0];
end
if(anim_flag)
    animate_vehicle(T,X,Y,X_c,Y_c, T_c,dist_hist,control_params_hist,control_input_hist, vehicle)
end
if(plot_flag)
    %% plots
    clf;
    subplot(2,4,1);
    title('wheel velocities');
    hold on;
    plotHelper(T,Rw*X(1,:),'s','R_w*\omega_{fl}',0);
    plotHelper(T,X(2,:),'d','Vx',0);
    grid on; legend show;

    subplot(2,4,2);
    title('body velocity')
    grid on;
    hold on;
    plotHelper(T,X(2,:),'o','V_x',5);

    subplot(2,4,3);
    title('inertial position')
    grid on;
    hold on;
    plotHelper(T,X(3,:),'o','X',5);

    subplot(2,4,4);
    title('longitudinal slip');
    grid on;
    hold on;
    plotHelper(T,Y(1,:),'sq','sfl',5);

    subplot(2,4,5);
    title('longitudinal force');
    grid on;
    hold on;
    plotHelper(T,Y(2,:),'sq','Fxfl',5);
    plotHelper(T,Y(4,:),'o','Fx tot',10);

    subplot(2,4,6);
    title('desired torques');
    grid on; hold on;
    plotHelper(control_input_hist.T_c,control_input_hist.Td(1,:),'s','des Td_{fl}',0);
    plotHelper(T,X(4,:),'s','Td_{fl}',20);
    plotHelper(control_input_hist.T_c,control_input_hist.Tb(1,:),'s','des Tb_{fl}',0);
    plotHelper(T,X(5,:),'s','Tb_{fl}',25);
    legend show;

    subplot(2,4,7)
    title('net torque')
    grid on; hold on;
    plotHelper(T,Y(5,:),'o','torquefl',0);

    subplot(2,4,8)
    title('V dot')
    grid on; hold on;
    plotHelper(T,Y(6,:),'o','Vx dot',0);
    plotHelper(T,Rw*Y(7,:),'o','wfl dot',0);
    legend show

end


%% ode wrapper
function [xdot,y] = vehicle_wrapper(t,x,params)
    % params is struct containing vehicle, control_input,dist, and constants
    vehicle = params.vehicle;
    control_input = params.control_input;
    control_params = params.control_params;
    dist = params.dist;
    constants = params.constants;

    % unpack vehicle states and control states
    x_v = x;
    [xdot_v,y_v] = one_wheel_model(t,x_v,vehicle,control_input,dist,constants);

    % combine states and outputs
    xdot = [xdot_v];
    y = [y_v];
end

% ode without controller states
function [xdot,y] = ode(t,x,params)
    % params is struct containing vehicle, control_input,dist, and constants
    vehicle = params.vehicle;
    control_input = params.control_input;
    control_params = params.control_params;
    dist = params.dist;
    constants = params.constants;

    [xdot,y] = one_wheel_model(t,x,vehicle,control_input,dist,constants);
end
