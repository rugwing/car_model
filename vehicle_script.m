% script that forms required vehicle struct

% TODO: should rewrite vehicle to be an object, doesn't change much in the code but allows on the fly adjustment of parameters rather than multiple scripts

%   vehicle - struct containing vehicle properties
%       L - length of vehicle
%       B - wheel base
%       Rw - wheel radius
%       L1 - dist from front axle to cg
%       L2 - dist from rear axle to cg
%       m - mass of vehicle
%       Iz - MOI of vehicle
%       Iw - MOI of wheel
%       @mu_slip_curve is a function handle pointing to a function:
%       inputs: slip s
%       outputs: [mu, Cf, Cr]
%       @drive_model function modelling (partially controlled) drivtrain:
%       inputs: current state of drive torque Td
%           desired/steady state drive torque Td_des
%       outputs: Td_dot
%       @brake_model function modelling (partially controlled) brakes:
%       inputs: current state of brake torque Tb
%           desired/ss brake torque Tb_des
%       outputs: Tb_dot

clear vehicle;
L = 2.04; B = 1.164; Rw = 13/39.7; L1 = 1.206; L2 = L-L1;
H = .55;
% L = .5; L1 = .2; L2 = L-L1;
m = 727; g = 9.81;
iyaw = .992;
Iz = iyaw*m*L1*L2;
Iw = 10; % guess
Wf = m*g*L2/L; % static front weight
Wr = m*g*L1/L; % static rear weight
mu_slip_curve = @(s)mu_slip_curve_vehicle(s);
% motor and brakes modelling
tau_motor = .05;
tau_brake = .05;
% this models the dynamics of the motor, not any of the torque-speed or power relationships, gearing, transmission,etc
drive_model = @(Td,Td_des)drive_model_vehicle(Td,Td_des,tau_motor);
brake_model = @(Tb,Tb_des)brake_model_vehicle(Tb,Tb_des,tau_brake);
% this models the controller for the motor, interpreting the given desired torque and translating that into the torque that the drivetrain can output
%   Rear wheel, 4x4, diff modeling should be placed here instead
%   Should be called in control loop, 
% abs slip range for allowable hysteresis range
% motor power rating
P_m = 70/1.341*1000; % small 100 hp engine?
T_stall = 1500; 
drivetrain_model = @(Td_com,X)drivetrain_model_vehicle(Td_com,X,P_m,T_stall);

% abs allowable range of slip for hysteresis
slip_range = [.1 .2];

% rolling resistance factor for vehicle
fr = .014;

vehicle = struct(...
    'L',L, ...
    'B',B, ...
    'H',H, ...
    'Rw',Rw, ...
    'L1',L1, ...
    'L2',L2, ...
    'm',m, ...
    'Iz',Iz, ...
    'Iw',Iw, ...
    'mu_slip_curve',mu_slip_curve, ...
    'drive_model',drive_model, ...
    'brake_model',brake_model, ...
    'slip_range',slip_range, ...
    'P_m',P_m, ...
    'T_stall',T_stall, ...
    'drivetrain_model',drivetrain_model, ...
    'fr',fr ...
    );
constants = struct('g',g);

% drivetrain modelling (rwd + diff,simple motor(no gears)
% inputs: commanded total drive torque, current state, power limit, stall torque
% outputs: 4x1 vector of desired torques to be fed into drive_model
function Td_des = drivetrain_model_vehicle(Td_com,X,P_m, T_stall)
    % keyboard
    % use simple dc motor as model: assume can always produce maximum power when not at stall torque
    % calculate maximum availiable torque as a function of omega, where omega is the speed of the diff: avg of two driven wheel omegas
    % driving rear two wheels with diff
    driven_w = [0 0 1 1];
    omega = X(1:4);
    omega_diff = driven_w*omega/sum(driven_w); % omega seen by motor
    omega_B = P_m/T_stall; % velocity when motor is no longer limited by stall torque
    if( abs(omega_diff) == 0 || abs(omega_diff) < omega_B ) % stall torque
        T_max = T_stall;
    else
        T_max = P_m/omega_diff; % maximum torque that the motor can output at this velocity
    end

    % if T_max is less than commanded torque, cannot deliver
    Td = min(T_max,Td_com);
    % translate back through diff, split by equal power 
    % if not moving, distribute torque equally
    if(abs(omega_diff)==0)
        Td_des = Td * driven_w' / sum(driven_w);
    else
        Td_des = (driven_w'.*omega)/sum(driven_w'.*omega) * Td;
    end
end

% drivetrain modelling as first order low pass filter
function Td_dot = drive_model_vehicle(Td,Td_des,tau)
    k = 1;
    Td_dot = 1/tau * [k.*Td_des - Td];
end

% brake modelling as first order low pass filter
function Tb_dot = brake_model_vehicle(Tb,Tb_des,tau)
    k = 1;
    Tb_dot = 1/tau * [k.*Tb_des - Tb];
end

% mu_slip_curve function for a particular vehicle
function [mu,CCf,CCr] = mu_slip_curve_vehicle(s)
    % since Cf and Cr are dependenton Wf Wr, rewrite function to return CCf and CCr instead; Cf and Cr will be calculated in four_wheel_model to take into account weight transfer
    s = abs(s);
    % CCf = 0.12 * 180/pi;
    % CCr = 0.14 * 180/pi;
    CCf_max = 0.12 * 180/pi;
    CCr_max = 0.14 * 180/pi;
    % assume cornering coefficient is max at no slip, zero at locked wheel
    % Cf_max = CCf;
    % Cr_max = CCr;
    
    % using mu-slip values in the onewheelbraking example
    slip_lookup = 0:.05:1;
    mu_max = 0.7;
    mu_lookup = mu_max*[0 .4 .8 .97 1 .98 .96 .94 .92 .9 .88 .855 .83 .81 .79 .77 .75 .73 .72 .71 .7];

    % lookup mu, Cf, and Cr
    mu = interp1(slip_lookup,mu_lookup,s,'Linear',mu_max);
    % should be replaced with an implementation of the friction ellipse?
    CCf = interp1([0,1]     ,[CCf_max 0],s,'Linear',CCf_max);
    CCr = interp1([0,1]     ,[CCr_max 0],s,'Linear',CCr_max);
end
