% on-off control of desired braking torque implementation of abs algorithm
% inputs:
%   x - vehicle state
%   Tb_input - input braking torque
%   vehicle - vehicle struct
%   control_input - control input struct for steering angles
% outputs:
%   Tb_des - transmitted braking torque
%   abs_flag - abs on/off boolean
function [Tb_des,abs_flag] = abs_control(x,Tb_input,vehicle,control_input)
    %% state and vehicle unpacking
    wfl = x(1); wfr = x(2); wrl = x(3); wrr = x(4);
    %       next three states are the velocities in each dof in the body frame
    Vx = x(5); Vy = x(6); omegaz = x(7);
    mu_slip_curve = vehicle.mu_slip_curve;
    B = vehicle.B; Rw = vehicle.Rw; L1 = vehicle.L1; L2 = vehicle.L2;
    m = vehicle.m; Iz = vehicle.Iz; Iw = vehicle.Iw; 
    deltaf = control_input.deltaf;
    deltar = control_input.deltar;

    s = zeros(1,4);
    %% compute slip at each wheel
    s(1) = (Rw*wfl - Vx*cos(deltaf))/max(Rw*wfl,Vx*cos(deltaf));
    s(2) = (Rw*wfr - Vx*cos(deltaf))/max(Rw*wfr,Vx*cos(deltaf));
    s(3) = (Rw*wrl - Vx*cos(deltar))/max(Rw*wrl,Vx*cos(deltar));
    s(4) = (Rw*wrr - Vx*cos(deltar))/max(Rw*wrr,Vx*cos(deltar));
    % if slip is non-existent (at rest) set to zero slip or if moving very slowly, assume no slip
    w = [wfl wfr wrl wrr];
    slip_nan_val = .000;
    V = sqrt(Vx^2 + Vy^2);
    V_min = .1;
s( isnan(s) | ((V<V_min)&(abs(Rw*w)<V_min)) ) = slip_nan_val;

    slip_range = vehicle.slip_range;
    % independent wheel abs implementation
    % hysteresis
    abs_flag = control_input.abs_flag;
    for(i=1:4)
        % normalization to range if velocities are in opposite directions
        if(s(i) < 0) % if slip is positive, should always engage brakes
            abs_flag(i) = 1;
        elseif(abs(s(i))>1)
            s(i) = sign(s(i));
        end
        % if slip exceeds allowable slip range, turn brakes off
        if( abs(s(i)) > slip_range(2))
            abs_flag(i) = 1;
        elseif( abs(s(i)) < slip_range(1))
            abs_flag(i) = 0;
        end
    end

    % apply abs to input braking torques
    Tb_des = ~(abs_flag).*Tb_input;
end
