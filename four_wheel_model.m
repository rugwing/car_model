% 2D vehicle model with braking and lateral forces % wheels are at fl, fr, rl, rr
% inputs:
% t - current time step
% x - current state:
    %   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit,Td(1:4),Tb(1:4)]';
%   vehicle - struct containing vehicle properties
%       L - length of vehicle
%       B - wheel base
%       Rw - wheel radius
%       L1 - dist from front axle to cg
%       L2 - dist from rear axle to cg
%       m - mass of vehicle
%       Iz - MOI of vehicle
%       Iw - MOI of wheel
%       @mu_slip_curve is a function handle pointing to a function:
%       inputs: slip s
%       outputs: [mu, Cf, Cr]
%       @drive_model 
%       inputs: state Td, desired torque Td_des
%       outputs: derivative Td_dot 
%       @brake_model
%       inputs: state Tb, desired torque Tb_des
%       outputs: derivative Tb_dot
%   control_input  - struct containing current time control input:
%                   Td - driving torque 4x1 vector applied to wheels
%                   Tb - braking torque 4x1 applied to wheels
%                   deltaf - front steer angle
%                   deltar - rear steer angle
%   dist - 2x1 vector of disturbance forces acting through cg at current time and grade in rad [Fx,Fy,grade]';
% constants - struct containing constants not related to vehicle:
%   g
% outputs:
% xdot - derivative of state
% y - additional outputs
function [xdot, y] = four_wheel_model(t,x,vehicle,control_input,dist,constants)
    % state x is:
    %   x= [wfl,wfr,wrl,wrr,Vx,Vy,omegaz,Xt,Yt,Psit]';
    %       first four states are the rotational velocities of each wheel
    wfl = x(1); wfr = x(2); wrl = x(3); wrr = x(4);
    %       next three states are the velocities in each dof in the body frame
    Vx = x(5); Vy = x(6); omegaz = x(7);
    %       next three states are the positions and heading in the inertial frame
    Xt = x(8); Yt = x(9); Psit = x(10);
    % additional states are the drive and brake torques
    Td = x(11:14);
    Tb = x(15:18);

    L = vehicle.L; fr = vehicle.fr; H = vehicle.H;
    B = vehicle.B; Rw = vehicle.Rw; L1 = vehicle.L1; L2 = vehicle.L2;
    m = vehicle.m; Iz = vehicle.Iz; Iw = vehicle.Iw; 
    g = constants.g;
    mu_slip_curve = vehicle.mu_slip_curve;
    drive_model = vehicle.drive_model;
    brake_model = vehicle.brake_model;

    deltaf = control_input.deltaf;
    deltar = control_input.deltar;
    % control input torques become desired torques to control to
    Td_des = control_input.Td;
    Tb_des = control_input.Tb;

    theta = dist(3); % grade angle

    V_min = .2; % limit to velocities for slip-no slip determination

    % all wheels have the same velocity in the body frame, that of the vehicle (Vx,Vy) but will depend on steer angle when computing slip

    %%
    % compute slip angle
    alphafl = deltaf - atan2(Vy+L1*omegaz,Vx);
    alphafr = deltaf - atan2(Vy+L1*omegaz,Vx);
    alpharl = deltar - atan2(Vy-L2*omegaz,Vx);
    alpharr = deltar - atan2(Vy-L2*omegaz,Vx);
    
    % min velocity for slip?
    % compute slip at each wheel
    sfl = (Rw*wfl - Vx*cos(deltaf))/max(Rw*wfl,Vx*cos(deltaf));
    sfr = (Rw*wfr - Vx*cos(deltaf))/max(Rw*wfr,Vx*cos(deltaf));
    srl = (Rw*wrl - Vx*cos(deltar))/max(Rw*wrl,Vx*cos(deltar));
    srr = (Rw*wrr - Vx*cos(deltar))/max(Rw*wrr,Vx*cos(deltar));
    % if slip is non-existent (at rest) set to small value of slip
    % for slow speeds, slip goes to 0 as well
    slip_nan_val = .00;
    if(isnan(sfl) || ( (abs(Vx) < V_min) && (abs(Rw*wfl) < V_min) ) )
        sfl = slip_nan_val;
    end
    if(isnan(sfr) || ( (abs(Vx) < V_min) && (abs(Rw*wfr) < V_min) ) )
        sfr = slip_nan_val;
    end
    if(isnan(srl) || ( (abs(Vx) < V_min) && (abs(Rw*wrl) < V_min) ) )
        srl = slip_nan_val;
    end
    if(isnan(srr) || ( (abs(Vx) < V_min) && (abs(Rw*wrr) < V_min) ) )
        srr = slip_nan_val;
    end
    if(abs(sfl)>1)
        sfl = sign(sfl);
    end
    if(abs(sfr)>1)
        sfr = sign(sfr);
    end
    if(abs(srl)>1)
        srl = sign(srl);
    end
    if(abs(srr)>1)
        srr = sign(srr);
    end


    % compute mu and cornering coefficient for each wheel
    [mufl,CCfl] = mu_slip_curve(sfl);
    [mufr,CCfr] = mu_slip_curve(sfr);
    [murl,~,CCrl] = mu_slip_curve(srl);
    [murr,~,CCrr] = mu_slip_curve(srr);

    % maintain traction if slow speed
    if(sqrt(Vx^2 + Vy^2) < V_min)
        [~,CCfmax,CCrmax] = mu_slip_curve(0);
        CCfl = CCfmax; CCfr = CCfmax;
        CCrl = CCrmax; CCrl = CCrmax;
    end

    % vectorize mu and CC; should have been done from the start!
    mu = [mufl mufr murl murr]';
    CC = [CCfl CCfr CCrl CCrr]';

    % include effects of grade on front-rear weight and cornering stiffness
    % weight transfer calculated by solving dynamic equations for (1D) case of 2 wheel vehicle going up a grade 
    % weight transfers require direction of slip to determine direction of forces
    slip_sign = sign([sfl sfr srl srr]');

    muf = mean(mu(1:2).*slip_sign(1:2));
    mur = mean(mu(3:4).*slip_sign(3:4));
    W_transfer_mat = [       1       1;
                        -L1-muf*H L2-mur*H];
    W_transfer_rhs = [m*g*cos(theta); 0];
    W_transfer_lon = inv(W_transfer_mat)*W_transfer_rhs; % longitudinal W_transfer F/R
    
    % approximate lateral weight transfer from a 2 wheel model and restricting to no roll
    % cl = mean(CC(1:2:end) .* [alphafl alpharl]' .* slip_sign(1:2:end));
    cl = mean(CC(1:2:end) .* [alphafl alpharl]');
    % cr = mean(CC(2:2:end) .* [alphafr alpharr]' .* slip_sign(2:2:end));
    cr = mean(CC(2:2:end) .* [alphafr alpharr]');
    W_transfer_mat_lat = [  1       1;
                        B/2+cl*H -B/2+cr*H];
    W_transfer_lat = inv(W_transfer_mat_lat)*W_transfer_rhs; % lateral weight transfer L/R
    % case if any are negative: this means that the car's weight is not enough to keep all 4 wheels on the ground! would need roll/pitch for reals there
    if(any(W_transfer_lat<0))
        W_transfer_lat = (W_transfer_lat > 0)*m*g;
    end
    if(any(W_transfer_lon<0))
        W_transfer_lon = (W_transfer_lon > 0)*m*g;
    end
    
    % extrapolate to weight on each wheel
    W_transfer =[W_transfer_lon(1)*W_transfer_lat(1);
                 W_transfer_lon(1)*W_transfer_lat(2);
                 W_transfer_lon(2)*W_transfer_lat(1);
                 W_transfer_lon(2)*W_transfer_lat(2);] /(m*g*cos(theta));

    Cfl = CCfl*W_transfer(1)/2;
    Cfr = CCfr*W_transfer(2)/2;
    Crl = CCrl*W_transfer(3)/2;
    Crr = CCrr*W_transfer(4)/2;

    % compute lateral forces from cornering stiffness and slip angles
    Fyfl = Cfl*alphafl;
    Fyfr = Cfr*alphafr;
    Fyrl = Crl*alpharl;
    Fyrr = Crr*alpharr;

    if(sqrt(Vx^2 + Vy^2) < V_min)
        Fyfl = 0; Fyfr = 0;
        Fyrl = 0; Fyrr = 0;
    end

    % compute traction forces from mu and normal forces
    % Fxfl = sign(sfl)*mufl*m*g*( L1/(L1+L2) ) * .5;
    % Fxfr = sign(sfr)*mufr*m*g*( L1/(L1+L2) ) * .5;
    % Fxrl = sign(srl)*murl*m*g*( L2/(L1+L2) ) * .5;
    % Fxrr = sign(srr)*murr*m*g*( L2/(L1+L2) ) * .5;
    Fxfl = sign(sfl)*mufl*W_transfer(1)/2;
    Fxfr = sign(sfr)*mufr*W_transfer(2)/2;
    Fxrl = sign(srl)*murl*W_transfer(3)/2;
    Fxrr = sign(srr)*murr*W_transfer(4)/2;
    % Fxfl = 0;
    % Fxfr = 0;
    % Fxrl = 0;
    % Fxrr = 0;

    % compute rolling resistance forces acting on cg
    %
    Fr = fr.*m.*g.*tanh(Vx);
    % Fr = 0*Fr;
    % compute grade force acting on cg
    Fg = m*g*sin(theta);
    
    % compute total torques acting on each wheel
    % driving_torque - tractive_torque - braking_torque
    torquefl = Td(1) - Fxfl*Rw - sign(wfl)*Tb(1);
    torquefr = Td(2) - Fxfr*Rw - sign(wfr)*Tb(2);
    torquerl = Td(3) - Fxrl*Rw - sign(wrl)*Tb(3);
    torquerr = Td(4) - Fxrr*Rw - sign(wrr)*Tb(4);

    
    % compute total forces acting on cg
    Fxtot = (Fxfl + Fxfr)*cos(deltaf) + (Fxrl + Fxrr)*cos(deltar) + ...
            -(Fyfl + Fyfr)*sin(deltaf) - (Fyrl + Fyrr)*sin(deltar) + dist(1) - Fr - Fg;
    Fytot = (Fyfl + Fyfr)*cos(deltaf) + (Fyrl + Fyrr)*cos(deltar) + ...
            (Fxfl + Fxfr)*sin(deltaf) + (Fxrl + Fxrr)*sin(deltar) + dist(2) ;

    % compute total moments acting on cg
    momentF = ( (Fyfl+Fyfr)*cos(deltaf) + (Fxfl+Fxfr)*sin(deltaf) ) * L1 + ...
              ( (Fxfr-Fxfl)*cos(deltaf) + (Fyfl-Fyfr)*sin(deltaf) ) * B/2;

    momentR = ( (Fyrl+Fyrr)*cos(deltar) + (Fxrl+Fxrr)*sin(deltar) ) * -L1 + ...
              ( (Fxrl-Fxrr)*cos(deltar) + (Fyrr-Fyrl)*sin(deltar) ) * -B/2;
    momentcg = momentF+momentR;

    % gather system equations
    wfl_dot = torquefl/Iw;
    wfr_dot = torquefr/Iw;
    wrl_dot = torquerl/Iw;
    wrr_dot = torquerr/Iw;

    Vx_dot = (m*Vy*omegaz + Fxtot)/m;
    Vy_dot = (-m*Vx*omegaz + Fytot)/m;
    % Vx_dot = ( Fxtot)/m;
    % Vy_dot = ( Fytot)/m;
    omegaz_dot = momentcg/Iz;

    Xt_dot = Vx*cos(Psit)-Vy*sin(Psit);
    Yt_dot = Vx*sin(Psit)+Vy*cos(Psit);
    Psit_dot = omegaz;

    % motor and brake derivatives
    Td_dot = drive_model(Td,Td_des);
    Tb_dot = brake_model(Tb,Tb_des);

    % slow speed chatter correction
    % assume no slip at slow speeds and vehicle dynamics that bring vehicle to a stop
    if(sqrt(Vx^2+Vy^2) < V_min)
        % first term ensures that Vx goes to zero
        if(sum(abs(Td)) > 1e-4)
            Vx_dot_no_slip = + (m + Iw/Rw^2)^-1 * (1/Rw*( max(0,sum(Td)-sum(Tb)) ));
            Vx_dot = max(Vx_dot,Vx_dot_no_slip);
        else
            % additional case if not driving, then drive velocity and omegas to zero
            Vx_dot = -Vx/V_min;
            Vy_dot = -Vy/V_min;
            omegaz_dot = -omegaz*2/V_min;
            wfl_dot = -wfl*2/V_min ;
            wfr_dot = -wfr*2/V_min ;
            wrl_dot = -wrl*2/V_min ;
            wrr_dot = -wrr*2/V_min ;
        end
    end

    % gather system derivative
    xdot = [wfl_dot, wfr_dot, wrl_dot, wrr_dot, ...
            Vx_dot, Vy_dot, omegaz_dot, ...
            Xt_dot, Yt_dot, Psit_dot, Td_dot', Tb_dot']';

    % output variables
    y = [sfl, sfr, srl, srr, ... % 1-4 - slip 
         alphafl, alphafr, alpharl, alpharr, ... % 5-8 - slip angles
         Fxfl, Fxfr, Fxrl, Fxrr, ... % 9-12 - longitudinal force on each wheel
         Fyfl, Fyfr, Fyrl, Fyrr, ... % 13-16 - lateral force on each wheel
         mufl, mufr, murl, murr, ... % 17-20 - mu
         Fytot, Fxtot, momentcg,Fr, ... % 21-24 - Body forces and rolling resistance
         W_transfer', ... % 25-28 - weight transfer per wheel
        ]';
    % if(srr == -1 && Fyrr~=0) %dummy
    %     keyboard
    % end
end
